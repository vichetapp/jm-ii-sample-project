import 'package:flutter/material.dart';

class FormWidgets extends StatefulWidget {
  @override
  _FormWidgetsState createState() => _FormWidgetsState();
}

class _FormWidgetsState extends State<FormWidgets> {
  var globalkey = GlobalKey<FormState>();
  var fnCon = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FormWidget"),
      ),
      body: Form(
          key: globalkey,
          child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: [buildFirstName(), buildSubmit()],
              ))),
    );
  }

  Widget buildFirstName() {
    return TextFormField(
      controller: fnCon,
      decoration: InputDecoration(
        hintText: "Please enter your first name",
        labelText: "First Name!",
        prefixIcon: Icon(Icons.person_pin_rounded),
        fillColor: Colors.yellow,
        filled: true,
        icon: Icon(Icons.label_important_outlined),
        prefix: Text("Pre-"),
        prefixStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: BorderSide(width: 5.0, color: Colors.green),
        ),
      ),
      validator: (value) {
        return (value.isEmpty) ? "Please enter first name?" : null;
      },
    );
  }

  Widget buildSubmit() {
    return ElevatedButton(
        onPressed: () {
          if (globalkey.currentState.validate()) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Hi ! ${fnCon.text}"),
            ));
          }
        },
        child: Text("Submit"));
  }
}
