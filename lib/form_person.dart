import 'package:flutter/material.dart';
import 'package:flutter_excercise/person.dart';
import 'package:flutter_excercise/person_widget.dart';

class FormPerson extends StatefulWidget {
  @override
  _FormPersonState createState() => _FormPersonState();
}

class _FormPersonState extends State<FormPerson> {
  Person people = Person.empty();

  var firstNameCon = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Person Info"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: PersonWidget(
          people: people,
        ),
      ),
    );
  }
}
