import 'package:flutter_excercise/model/crud_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert'; //for json_decode

class CrudAPI {
  static String url =
      "https://api.themoviedb.org/3/movie/550?api_key=557ecb9e32bf45cbb92c03493c5aaa92";
  String address;
  String httpAddress;

  set setUrl(String address) {
    this.address = address;
  }

  String get getUrl => this.address;
  set setHttpAddress(String http) {
    this.httpAddress = http;
  }

  String get getHttpAddress => httpAddress;

  static Future<String> getAlls() async {
    print('getAll Data from server');
    // final response = await http.get("https://api.themoviedb.org/3/movie/550?api_key=557ecb9e32bf45cbb92c03493c5aaa92");
    http.Response response = await http.get(
        Uri.https("https://api.mocki.io/v1/13f44462", ""),
        headers: {"Accept": "application/json"});
    // final response =
    //     await http.get(Uri.http("https://api.mocki.io/v1/13f44462", ""));
    // http.Response response = http.read(Uri.http(url));
    // final response = await http.get("http://192.168.1.12/bibapp/getCrud.php");
    // if (response.statusCode == 200) {
    //   print('statuscode is 200');
    //   final crudJsons = jsonDecode(response.body); //json.Decore(response.body);
    //   List<CRUDModel> list = [];
    //   print("response status code = 200");
    //   for (final crudJson in crudJsons) {
    //     final crud = CRUDModel.fromJson(crudJson);
    //     list.add(crud);
    //   }
    //   print("crud added to list already!");
    //   return list;
    // } else {
    //   print('statuscode is not 200');
    //   return [];
    // }
    return 'ok';
  }
}
