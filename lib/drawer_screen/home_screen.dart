import 'package:flutter/material.dart';
import 'package:flutter_excercise/screen/box_constraint_widget.dart';
import 'package:flutter_excercise/screen/card_widget.dart';
import 'package:flutter_excercise/screen/container_widget.dart';
import 'package:flutter_excercise/screen/dialog_widget.dart';
import 'package:flutter_excercise/screen/expanded_widget.dart';
import 'package:flutter_excercise/screen/expaned_widget_text.dart';
import 'package:flutter_excercise/screen/expansion_panel_widget.dart';
import 'package:flutter_excercise/screen/flexiable_widget.dart';
import 'package:flutter_excercise/screen/form_detail.dart';
import 'package:flutter_excercise/screen/gesture_detector_widget.dart';
import 'package:flutter_excercise/screen/grid_view_widget.dart';
import 'package:flutter_excercise/screen/image_loading_widget.dart';
import 'package:flutter_excercise/screen/mainaxis_space.dart';
import 'package:flutter_excercise/screen/navigator_page_route.dart';
import 'package:flutter_excercise/screen/padding_widget.dart';
import 'package:flutter_excercise/screen/pageviewer.dart';
import 'package:flutter_excercise/screen/popmenubutton_widget.dart';
import 'package:flutter_excercise/screen/radio_widget_snackbar.dart';
import 'package:flutter_excercise/screen/simple_dialog_widget.dart';
import 'package:flutter_excercise/screen/single_scroll_widget.dart';
import 'package:flutter_excercise/screen/spacer_widget.dart';
import 'package:flutter_excercise/screen/stack_widget.dart';
import 'package:flutter_excercise/screen/statefull_widget_flower.dart';
import 'package:flutter_excercise/screen/stateless_widget.dart';
import 'package:flutter_excercise/screen/tabbarview_widget.dart';
import 'package:flutter_excercise/screen/text_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;
  List<Widget> listScreen = [
    GridViewWidget(),
    BoxConstraintWidget(),
    CardWidget(),
    ContainerWidget(),
    ExpanedWidget(),
    FlexibleWidget(),
    GestureDetectorWidget(),
    ImageLoading(),
    MainAxisSpaceEvenly(),
    PaddingWidget(),
    SingleScrollWidget(),
    StackWidget(),
    FlowerWidget(),
    StatelessDemo(),
    TextWidget(),
    DialogWidget(),
    ExpandedText(),
    GridViewWidget(),
    PopupMenuWidget(),
    RadioWidgetSnackbar(),
    SpacerWidget(),
    TabBarViewWidget(),
    ExpansionPanelListWidget(),
    SimpleDialogWidget(),
    NavigatorPageRoute(),
    PageViewerWidget(),
    FormDetail(),
  ];
  List<String> listTitle = [
    "Other",
    "1. BoxConstraint",
    "2. Card",
    "3. Container",
    "4. Expaned",
    "5. Flexible",
    "6. GestureDetector",
    "7. Image Loading",
    "8. MainAxis",
    "9. Padding",
    "10. SingleScroll",
    "11. Stack",
    "12. Statefull",
    "13. Stateless",
    "14. TextWidget",
    "15. AlertDialog",
    "16. Texting",
    "17. GridView",
    "18. PopupMenu",
    "19. Radio and Snackbar",
    "20. Spacer",
    "21. TabBarView",
    "22. ExpansionaPanel",
    "23. SimpleDialog",
    "24. Navigator Page Route",
    "25. PageView",
    "26. FormField"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: ListView.builder(
      reverse: true,
      itemCount: listTitle.length,
      itemBuilder: (context, index) {
        return ListTile(
          onTap: () {
            setState(() {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) {
                  return listScreen[index];
                },
              ));
              currentIndex = index;
            });
          },
          // leading: Icon(Icons.widgets),
          // leading: CircleAvatar(
          //   backgroundImage: AssetImage("images/data.png"),
          // ),
          leading: Container(
            child: Image.asset("images/data.png"),
          ),
          title: Text(listTitle[index]),
          trailing:
              IconButton(icon: Icon(Icons.arrow_forward_ios), onPressed: () {}),
        );
      },
    )));
  }
}
