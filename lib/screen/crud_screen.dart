import 'package:flutter/material.dart';
import 'package:flutter_excercise/api/crud_api.dart';

class CrudScreen extends StatefulWidget {
  @override
  _CrudScreenState createState() => _CrudScreenState();
}

class _CrudScreenState extends State<CrudScreen> {
  Widget getCrud() {
    return Container(
      child: FutureBuilder(
        future: CrudAPI.getAlls(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return Text("error loading");
            } else if (snapshot.data == null) {
              return Text("no data record on server!");
            } else {
              return buildCrudData(snapshot.data);
            }
          }
        },
      ),
    );
  }

  Widget buildCrudData(List data) {
    return (data.length == 0)
        ? Text("no data")
        : ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              return Container(
                child: Text("Working"),
              );
            },
          );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("CRUD"),
        ),
        body: getCrud());
  }
}
