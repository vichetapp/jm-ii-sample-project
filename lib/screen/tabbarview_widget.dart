import 'package:flutter/material.dart';

class TabBarViewWidget extends StatefulWidget {
  @override
  _TabBarViewWidgetState createState() => _TabBarViewWidgetState();
}

class _TabBarViewWidgetState extends State<TabBarViewWidget> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("TabView"),
          bottom: TabBar(
            tabs: [
              Tab(
                child: Text("One"),
                icon: Icon(Icons.people),
              ),
              Tab(
                child: Text("Two"),
                icon: Icon(Icons.open_with),
              ),
              Tab(
                child: Text("Three"),
                icon: Icon(Icons.open_in_browser),
              )
            ],
          ),
        ),
        body: TabBarView(children: [TabOne(), TabTwo(), TabThree()]),
        bottomSheet: TabBar(
          tabs: [
            Tab(
              child: Text(
                "One",
                style: TextStyle(color: Colors.green),
              ),
              icon: Icon(Icons.people, color: Colors.green),
            ),
            Tab(
              child: Text("Two", style: TextStyle(color: Colors.green)),
              icon: Icon(Icons.open_with, color: Colors.green),
            ),
            Tab(
              child: Text("Three", style: TextStyle(color: Colors.green)),
              icon: Icon(Icons.open_in_browser, color: Colors.green),
            )
          ],
        ),
      ),
    );
  }
}

class TabOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.green,
          image: DecorationImage(image: AssetImage("images/kitty01.jpg"))),
      child: Text("One"),
    );
  }
}

class TabTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.blue,
          image: DecorationImage(image: AssetImage("images/kitty02.jpg"))),
      child: Text("Two"),
    );
  }
}

class TabThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.red,
          image: DecorationImage(image: AssetImage("images/kitty03.jpg"))),
      child: Text("Three"),
    );
  }
}
