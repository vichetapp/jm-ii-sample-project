import 'package:flutter/material.dart';

class SimpleDialogWidget extends StatefulWidget {
  @override
  _SimpleDialogWidgetState createState() => _SimpleDialogWidgetState();
}

class _SimpleDialogWidgetState extends State<SimpleDialogWidget> {
  BoxFit box = BoxFit.none;
  @override
  Widget build(BuildContext context) {
    List<Widget> listImage = [];
    for (int i = 0; i < 8; i++) {
      listImage.add(GridTile(
          header: GridTileBar(
            backgroundColor: Color.fromRGBO(0, 0, 100, 0.2),
            title: Text("kitty0$i.jpg"),
          ),
          child: Container(
              decoration: BoxDecoration(border: Border.all()),
              child: Image.asset("images/kitty0$i.jpg", fit: box))));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("SimpleDilog"),
        actions: [
          IconButton(
              icon: Icon(Icons.read_more),
              onPressed: () {
                showSimpleDiloag();
              })
        ],
      ),
      body: Container(
          child: GridView.count(
        crossAxisCount: 2,
        children: listImage,
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showSimpleDiloag();
        },
        child: Icon(Icons.refresh),
      ),
    );
  }

  void showSimpleDiloag() async {
    BoxFit b = await showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text("please select boxfit"),
          children: [
            SimpleDialogOption(
              child: Text("boxfit.contain"),
              onPressed: () {
                Navigator.pop(context, BoxFit.contain);
              },
            ),
            SimpleDialogOption(
              child: Text("boxfit.cover"),
              onPressed: () {
                Navigator.pop(context, BoxFit.cover);
              },
            ),
            SimpleDialogOption(
              child: Text("boxfit.fill"),
              onPressed: () {
                Navigator.pop(context, BoxFit.fill);
              },
            ),
            SimpleDialogOption(
              child: Text("boxfit.fitHeight"),
              onPressed: () {
                Navigator.pop(context, BoxFit.fitHeight);
              },
            ),
            SimpleDialogOption(
              child: Text("boxfit.fitWidth"),
              onPressed: () {
                Navigator.pop(context, BoxFit.fitWidth);
              },
            ),
            SimpleDialogOption(
              child: Text("boxfit.none"),
              onPressed: () {
                Navigator.pop(context, BoxFit.none);
              },
            ),
          ],
        );
      },
    );
    if (b != null) {
      setState(() {
        box = b;
      });
    }
  }
}
