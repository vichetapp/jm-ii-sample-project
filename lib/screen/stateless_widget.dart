import 'package:flutter/material.dart';

class StatelessDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Stateless')),
      body: ListView(
        children: [
          BuildCarWidget(
              make: "Toyota",
              model: "Fortuner",
              imageSrc:
                  "https://new-tmp-website-2019-s3.s3.ap-southeast-1.amazonaws.com/uploads/prices/563/002_563_1604455113574_000.png"),
          BuildCarWidget(
              make: "Toyota",
              model: "Prius",
              imageSrc:
                  "https://images.squarespace-cdn.com/content/v1/57019c4c04426228e6c546cd/1476996747833-BHQ4E2KR8QZYPLNM9WC0/ke17ZwdGBToddI8pDm48kMQs2m2ijENlnRCI35T13wF7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UTr2aYjkU7xokKIF1ZtuayP4kRB1FwUVOHOsvzUQCA3CbSexTd1-frD7527z4SM9QQ/image-asset.png"),
          BuildCarWidget(
            make: "Toyota",
            model: "Hilux Vigo",
            imageSrc:
                "https://i.pinimg.com/originals/d7/0e/04/d70e04249725eea48b46fd4c6a803508.png",
          ),
          Image.asset("images/kuro2020.png")
        ],
      ),
    );
  }
}

class BuildCarWidget extends StatelessWidget {
  final String make;
  final String model;
  final String imageSrc;
  BuildCarWidget({this.make, this.model, this.imageSrc});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(border: Border.all()),
        child: Column(
          children: [
            Text(make, style: TextStyle(color: Colors.green, fontSize: 20.0)),
            Text(model),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Image.network(imageSrc),
            )
          ],
        ),
      ),
    );
  }
}
