import 'package:flutter/material.dart';
import 'package:flutter_excercise/model/customer.dart';
import 'package:flutter_excercise/model/order.dart';
import 'package:flutter_excercise/screen/customer_detail_widget.dart';

class NavigatorPageRoute extends StatefulWidget {
  @override
  _NavigatorPageRouteState createState() => _NavigatorPageRouteState();
}

class _NavigatorPageRouteState extends State<NavigatorPageRoute> {
  List<Customer> customers = [
    Customer("Sok kha", "Phnom Penh", [
      Order("Cocacol", "Beverage Drinking", DateTime.now(), 12),
      Order("ABC", "Acohle Drinking", DateTime.now(), 2),
      Order("Corona", "Beverage Drinking", DateTime.now(), 33),
      Order("Twite", "Beverage Drinking", DateTime.now(), 14),
    ]),
    Customer("Tong LOng", "Battam Bang", [
      Order("Tiger", "Acohle Drinking", DateTime.now(), 76),
      Order("Pepsi", "Beverage Drinking", DateTime.now(), 22),
    ]),
    Customer("Mean Heng", "Takeo", [
      Order("Samaria", "Beverage Drinking", DateTime.now(), 32),
      Order("Cocacola", "Beverage Drinking", DateTime.now(), 52),
    ]),
    Customer("Mey mey", "Kandal", [
      Order("Twist", "Beverage Drinking", DateTime.now(), 32),
      Order("Cocacola", "Beverage Drinking", DateTime.now(), 52),
      Order("7up", "Beverage Drinking", DateTime.now(), 2),
    ]),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("NavigatorPageRoute"),
      ),
      body: ListView.builder(
        itemCount: customers.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: Icon(Icons.integration_instructions_outlined),
            title: Text(customers[index].getName),
            subtitle: Text(customers[index].getLocation),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              onTapDetail(index);
            },
          );
        },
      ),
    );
  }

  onTapDetail(int index) {
    // Navigator.push(context, MaterialPageRoute(
    //   builder: (context) {return CustomerDetail(customer: customers[index]);},
    // ));
    Navigator.pushNamed(context, "/orderDetails", arguments: customers[index]);
  }
}
