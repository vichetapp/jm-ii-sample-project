import 'dart:math';

import 'package:flutter/material.dart';

class StackWidget extends StatefulWidget {
  @override
  _StackWidgetState createState() => _StackWidgetState();
}

class _StackWidgetState extends State<StackWidget> {
  Random rand = Random();
  int next(int min, int max) {
    return min + rand.nextInt(max);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Stack"),
      ),
      body: Stack(
        children: [
          Positioned(
              top: 0,
              left: 0,
              child: Container(
                  width: 100,
                  height: 100,
                  color: Color.fromRGBO(
                      next(0, 255), next(0, 255), next(0, 255), 0.5))),
          Positioned(
              top: 50,
              left: 50,
              child: Container(
                  width: 100,
                  height: 100,
                  color: Color.fromRGBO(
                      next(0, 255), next(0, 255), next(0, 255), 0.5))),
          Positioned(
              top: 100,
              left: 100,
              child: Container(
                  width: 100,
                  height: 100,
                  color: Color.fromRGBO(
                      next(0, 255), next(0, 255), next(0, 255), 0.5))),
          Positioned(
            top: 150,
            left: 150,
            child: Container(
                width: 100,
                height: 100,
                color: Color.fromRGBO(
                    next(0, 255), next(0, 255), next(0, 255), 0.5)),
          )
        ],
      ),
    );
  }
}
