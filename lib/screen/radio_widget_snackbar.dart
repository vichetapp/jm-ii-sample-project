import 'package:flutter/material.dart';

class RadioWidgetSnackbar extends StatefulWidget {
  @override
  _RadioWidgetSnackbarState createState() => _RadioWidgetSnackbarState();
}

class _RadioWidgetSnackbarState extends State<RadioWidgetSnackbar> {
  List<String> list = ["Database", "Network", "Programming", "Mobile App"];
  String selected = "Database";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Radio"),
      ),
      body: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return ListTile(
            onTap: () {
              setState(() {
                selected = list[index];
                showSnackBar(context);
              });
            },
            leading: Radio(
              value: list[index],
              groupValue: selected,
              onChanged: (value) {
                print("value is $value");
                setState(() {
                  selected = value;
                  showSnackBar(context);
                });
              },
            ),
            title: Text("${list[index]}"),
          );
        },
      ),
    );
  }

  showSnackBar(BuildContext context) {
    var snack = SnackBar(
      backgroundColor: Color.fromRGBO(0, 0, 0, 0.1),
      content: Text("You are selected!  $selected"),
      elevation: 10.0,
      action: SnackBarAction(
          textColor: Colors.white,
          label: "OK",
          onPressed: () {
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
          }),
    );
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}
