import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class ImageLoading extends StatefulWidget {
  @override
  _ImageLoadingState createState() => _ImageLoadingState();
}

class _ImageLoadingState extends State<ImageLoading> {
  bool loadImageAsset = false;
  bool loadNetworkImage = false;
  bool loadImageFile = false;
  bool loadImageMemory = false;
  Uint8List imageData;

  void initState() {
    print("intialState executed");
    loadImageFromMemory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Image"),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              loadImageAsset
                  ? Container(
                      height: 300,
                      width: 200,
                      child: Image.asset("images/kuro2020.png"))
                  : Text("no Image from ImageAsset"),
              loadNetworkImage
                  ? Container(
                      width: 300,
                      height: 200,
                      child: Image.network(
                          "https://www.lexus.com/cm-img/mpr/lx/2021/visualizer/570-three-row/exterior/20-split-five-spoke/eminent-white-pearl/small-3.jpg"),
                    )
                  : Text("no image from NetworkImage"),
              loadImageFile
                  ? FadeInImage.assetNetwork(
                      placeholder: "images/loading.gif",
                      image:
                          "https://miro.medium.com/max/1000/1*ilC2Aqp5sZd1wi0CopD1Hw.png",
                    )
                  : Text("no image from File"),
              loadImageMemory
                  ? imageData == null
                      ? CircularProgressIndicator()
                      : Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: MemoryImage(imageData, scale: 0.5))),
                        )
                  : Text(""),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      loadImageAsset = true;
                    });
                  },
                  child: Text("Load Image from asset")),

              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      loadNetworkImage = true;
                    });
                  },
                  child: Text("Load Image from Network")),
              //
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      loadImageFile = true;
                    });
                  },
                  child: Text("Load Image from File")),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      loadImageMemory = true;
                    });
                  },
                  child: Text("Load Image from Memory")),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            loadImageAsset = false;
            loadImageFile = false;
            loadNetworkImage = false;
          });
        },
        child: Icon(Icons.refresh),
      ),
    );
  }

  void loadImageFromMemory() async {
    Uint8List data =
        (await rootBundle.load("images/kuro2020.png")).buffer.asUint8List();
    setState(() {
      imageData = data;
    });
  }
}
