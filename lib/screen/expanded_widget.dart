import 'package:flutter/material.dart';

class ExpanedWidget extends StatefulWidget {
  @override
  _ExpanedWidgetState createState() => _ExpanedWidgetState();
}

class _ExpanedWidgetState extends State<ExpanedWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Expaned"),
        ),
        body: Center(
          child: Column(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    width: 100,
                    height: 100,
                    color: Colors.red,
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    width: 100,
                    height: 100,
                    color: Colors.green,
                  )),
              Expanded(
                  flex: 2,
                  child: Container(
                    width: 100,
                    height: 100,
                    color: Colors.orange,
                  )),
            ],
          ),
        ));
  }
}
