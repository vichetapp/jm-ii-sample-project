import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

class PaddingWidget extends StatefulWidget {
  @override
  _PaddingWidgetState createState() => _PaddingWidgetState();
}

class _PaddingWidgetState extends State<PaddingWidget> {
  int index = 0;
  static const double TWENTY = 20.0;
  List<String> title = [
    'all 20.0',
    'left 20.0',
    'right 20.0',
    'bottom 20.0',
    'top 20.0'
        ''
  ];
  List<EdgeInsets> listEdget = [
    EdgeInsets.all(TWENTY),
    EdgeInsets.only(left: TWENTY),
    EdgeInsets.only(right: TWENTY),
    EdgeInsets.only(bottom: TWENTY),
    EdgeInsets.only(top: TWENTY)
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title[index]),
        actions: [
          IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                setState(() {
                  index++;
                  if (index == listEdget.length) {
                    index = 0;
                  }
                });
              })
        ],
      ),
      body: Padding(
        padding: listEdget[index],
        child: Container(
          color: Colors.green,
        ),
      ),
    );
  }
}
