import 'package:flutter/material.dart';

class BoxConstraintWidget extends StatefulWidget {
  @override
  _BoxConstraintWidgetState createState() => _BoxConstraintWidgetState();
}

class _BoxConstraintWidgetState extends State<BoxConstraintWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BoxConstrain"),
      ),
      body: Center(
        child: ConstrainedBox(
          child: Container(
            child: Text("ttesting testing testing testing testing "),
            decoration: BoxDecoration(border: Border.all()),
          ),
          constraints: BoxConstraints(
              minWidth: 50, maxWidth: 200, minHeight: 50, maxHeight: 200),
        ),
      ),
    );
  }
}
