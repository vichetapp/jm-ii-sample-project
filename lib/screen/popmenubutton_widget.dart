import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PopupMenuWidget extends StatefulWidget {
  @override
  _PopupMenuWidgetState createState() => _PopupMenuWidgetState();
}

enum POPMENU { ADD_1, ADD_10, ADD_100, EXIT }

class _PopupMenuWidgetState extends State<PopupMenuWidget> {
  int counter = 0;
  increament(num) {
    setState(() {
      counter += num;
    });
  }

  exitApp() {
    SystemNavigator.pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PopupMenu"),
        actions: [
          PopupMenuButton(
            onSelected: (selected) {
              switch (selected) {
                case POPMENU.ADD_1:
                  increament(1);
                  break;
                case POPMENU.ADD_10:
                  increament(10);
                  break;
                case POPMENU.ADD_100:
                  increament(100);
                  break;
                default:
                  exitApp();
                  break;
              }
            },
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text("+1"),
                  value: POPMENU.ADD_1,
                ),
                PopupMenuItem(
                  child: Text("+10"),
                  value: POPMENU.ADD_10,
                ),
                PopupMenuItem(
                  child: Text("+100"),
                  value: POPMENU.ADD_100,
                ),
                PopupMenuItem(
                  child: Text("Exit"),
                  value: POPMENU.EXIT,
                )
              ];
            },
          ),
        ],
      ),
      body: Center(
        child: Container(
          child: Text(
            "$counter",
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
      ),
    );
  }
}
