import 'package:flutter/material.dart';

class ExpandedText extends StatefulWidget {
  @override
  _ExpandedTextState createState() => _ExpandedTextState();
}

class _ExpandedTextState extends State<ExpandedText> {
  bool top = true;
  bool bottom = true;

  Widget topCon() {
    return Container(
      child: Text("Expanded top"),
      decoration: BoxDecoration(
          color: Colors.yellow,
          border: Border.all(width: 5.0, color: Colors.blue)),
    );
  }

  Widget bottomCon() {
    return Container(
      child: Text("Expanded bottom"),
      decoration: BoxDecoration(
          color: Colors.pink,
          border: Border.all(width: 5.0, color: Colors.red)),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget topWidget = top ? Expanded(child: topCon()) : topCon();
    Widget bottomWidget = bottom ? Expanded(child: bottomCon()) : bottomCon();
    return Scaffold(
      appBar: AppBar(
        title: Text("Expanded"),
        actions: [
          Center(
            child: Text("Top"),
          ),
          IconButton(
              icon: Icon(Icons.expand_less),
              onPressed: () {
                setState(() {
                  top = !top;
                });
              }),
          Center(
            child: Text("Bottom"),
          ),
          IconButton(
              icon: Icon(Icons.expand_more),
              onPressed: () {
                setState(() {
                  bottom = !bottom;
                });
              }),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: [
            topWidget,
            bottomWidget,
          ],
        ),
      ),
    );
  }
}
