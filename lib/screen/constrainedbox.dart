import 'package:flutter/material.dart';

class ConstrainedDemo extends StatefulWidget {
  @override
  _ConstrainedDemoState createState() => _ConstrainedDemoState();
}

class _ConstrainedDemoState extends State<ConstrainedDemo> {
  int counter = 0;
  List<Widget> listNum;

  @override
  Widget build(BuildContext context) {
    listNum = [];
    for (int i = 0; i < counter; i++) {
      listNum.add(Container(
        child: Text("num $i"),
      ));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Counter"),
      ),
      body: Center(
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 300, maxHeight: 500),
          child: Container(
              decoration: BoxDecoration(border: Border.all()),
              child: ListView(
                children: listNum,
              )),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            setState(() {
              counter++;
            });
          }),
    );
  }
}
