import 'package:flutter/material.dart';

class ButtonWidget extends StatefulWidget {
  @override
  _ButtonWidgetState createState() => _ButtonWidgetState();
}

class _ButtonWidgetState extends State<ButtonWidget> {
  List<String> list = ["one", "Two", "three"];
  String selected = "one";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Button"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          buildFloatbutton(),
          buildElevatedButton(),
          buildIconButton(),
          buildOutlineButton(),
          buildDropdownButton(),
          buildBackButton(),
          buildCloseButton(),
        ],
      ),
      floatingActionButton: buildFloationAction(),
    );
  }

  Widget buildFloatbutton() {
    return Row(
      children: [
        Text("TextButton"),
        TextButton(
            onPressed: () => print("clicked"), child: Text("TextButton")),
      ],
    );
  }

  Widget buildElevatedButton() {
    return Row(
      children: [
        Text("RaisedButton"),
        ElevatedButton(
            onPressed: () => print("Clicked ElevationButton"),
            child: Text("ElevatedButton"))
      ],
    );
  }

  Widget buildIconButton() {
    return Row(children: [
      Text("IconButton"),
      IconButton(
        icon: Icon(Icons.ac_unit),
        onPressed: () => print("clicked me"),
      )
    ]);
  }

  Widget buildOutlineButton() {
    return Row(
      children: [
        Text("OutlineButton"),
        OutlinedButton(
            style: ButtonStyle(
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0))),
                side: MaterialStateProperty.all(BorderSide(width: 5.0)),
                textStyle: MaterialStateProperty.all(
                    TextStyle(fontSize: 20.0, fontFamily: "Lucida")),
                elevation: MaterialStateProperty.all(12.0),
                foregroundColor: MaterialStateProperty.all(Colors.pink),
                overlayColor: MaterialStateProperty.all(Colors.blue),
                padding: MaterialStateProperty.all(EdgeInsets.all(18.0)),
                shadowColor: MaterialStateProperty.all(Colors.yellow),
                backgroundColor: MaterialStateProperty.all(Colors.yellow)),
            onPressed: () => print("outlinebutton"),
            child: Text("OutlineButton"))
      ],
    );
  }

  Widget buildDropdownButton() {
    return Row(children: [
      Text("Dropdown"),
      DropdownButton(
        // itemHeight: 10.0,
        isDense: true,
        onChanged: (select) {
          setState(() {
            print("selected is $select");
            selected = select;
          });
        },
        value: selected,
        selectedItemBuilder: (context) {
          return list
              .map((e) => Container(width: 100.0, height: 40.0, child: Text(e)))
              .toList();
        },
        items: list
            .map((e) => DropdownMenuItem(
                  child: Text("selectc $e"),
                  value: e,
                ))
            .toList(),
      )
    ]);
  }

  Widget buildBackButton() {
    return Row(
      children: [
        Text("Back Button"),
        BackButton(
          onPressed: () {
            print("clicek back");
          },
        )
      ],
    );
  }

  Widget buildCloseButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text("CloseButton "),
        CloseButton(
          onPressed: () {
            print("click closed");
          },
        )
      ],
    );
  }

  Widget buildFloationAction() {
    return FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          print("clicked floating button");
        });
  }
}
