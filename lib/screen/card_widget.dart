import 'package:flutter/material.dart';

class CardWidget extends StatefulWidget {
  @override
  _CardWidgetState createState() => _CardWidgetState();
}

class _CardWidgetState extends State<CardWidget> {
  List<Widget> listCard = [
    NewsCard(
      news: News(
          logo: "images/bbc.jpg",
          date: DateTime.now(),
          title: "Title",
          content: "Content"),
    ),
    NewsCard(
      news: News(
          logo: "images/bbc.jpg",
          date: DateTime.now(),
          title: "Title 2",
          content: "Content 2"),
    )
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Card"),
        ),
        body: Padding(
          padding: EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: listCard,
            ),
          ),
        ));
  }
}

class News {
  String logo;
  DateTime date;
  String title;
  String content;

  News({this.logo, this.date, this.title, this.content});
}

class NewsCard extends StatelessWidget {
  final News news;
  NewsCard({this.news});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Card(
        color: Colors.yellow,
        elevation: 10.0,
        margin: EdgeInsets.all(16.0),
        shadowColor: Colors.red,
        // semanticContainer: true,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Image.asset(
                "${news.logo}",
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:
                  Text("${news.date.day}/${news.date.month}/${news.date.year}"),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "${news.title}",
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("${news.content}"),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(onPressed: () {}, child: Text("Share")),
                ElevatedButton(onPressed: () {}, child: Text("Bookmark")),
                ElevatedButton(onPressed: () {}, child: Text("Link"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
