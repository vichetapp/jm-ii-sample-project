import 'package:flutter/material.dart';

class SingleScrollWidget extends StatefulWidget {
  @override
  _SingleScrollWidgetState createState() => _SingleScrollWidgetState();
}

class _SingleScrollWidgetState extends State<SingleScrollWidget> {
  List<Widget> list = [];

  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < 20; i++) {
      list.add(Center(
        child: Container(
          margin: EdgeInsets.all(8.0),
          // padding: EdgeInsets.all(8.0),
          decoration:
              BoxDecoration(border: Border.all(width: 2, color: Colors.red)),
          child: Text("$i"),
        ),
      ));
    }
    return Scaffold(
        appBar: AppBar(
          title: Text("SingleScrollView"),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: list,
          ),
        ));
  }
}
