import 'package:flutter/material.dart';
import 'package:flutter_excercise/model/customer.dart';
import 'package:flutter_excercise/model/order.dart';

class CustomerDetail extends StatefulWidget {
  final Customer customer;
  CustomerDetail({this.customer});
  @override
  _CustomerDetailState createState() => _CustomerDetailState();
}

class _CustomerDetailState extends State<CustomerDetail> {
  bool isExpan = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Customer Detail")),
      body: SingleChildScrollView(
        child: ExpansionPanelList(
          expansionCallback: (panelIndex, isExpanded) {
            setState(() {
              isExpan = !isExpanded;});},
          children: [
            ExpansionPanel(
              isExpanded: isExpan,
              headerBuilder: (context, isExpanded) {
                return Padding(
                    padding: EdgeInsets.all(8.0),
                    child: ListTile(
                      title: Text(widget.customer.getName),
                      subtitle: Text(widget.customer.getLocation),
                    ));
              },
              body: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: displayOrderList(widget.customer.getOrderItems),
                ),),)],
        ),
      ),
    );
  }

  List<Widget> displayOrderList(List<Order> list) {
    List<Widget> listItem = [];
    for (int i = 0; i < list.length; i++) {
      listItem.add(Text("Item #$i = ${list[i].getItem}"));
    }
    return listItem;
  }
}
