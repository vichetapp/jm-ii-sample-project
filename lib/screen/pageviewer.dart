import 'package:flutter/material.dart';

class PageViewerWidget extends StatefulWidget {
  @override
  _PageViewerWidgetState createState() => _PageViewerWidgetState();
}

class _PageViewerWidgetState extends State<PageViewerWidget> {
  List<Widget> list = [
    Pages(
      text: "Page 1",
      col: Colors.blue,
    ),
    Pages(
      text: "Page 2",
      col: Colors.red,
    ),
    Pages(
      text: "Page 3",
      col: Colors.green,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PageView"),
      ),
      body: PageView.builder(
        // physics: BouncingScrollPhysics(),
        itemCount: list.length,
        itemBuilder: (context, index) {
          return list[index];
        },
        onPageChanged: (index) {
          print("page $index");
        },
      ),
    );
  }
}

class Pages extends StatelessWidget {
  final String text;
  final Color col;
  Pages({this.text, this.col});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: col),
      child: Text("Page $text"),
    );
  }
}
