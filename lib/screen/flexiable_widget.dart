import 'package:flutter/material.dart';

class FlexibleWidget extends StatefulWidget {
  @override
  _FlexibleWidgetState createState() => _FlexibleWidgetState();
}

class _FlexibleWidgetState extends State<FlexibleWidget> {
  bool topTightFit = false;
  bool topExpad = false;
  String tight = "Tight";
  String expanded = "expanded";
  @override
  Widget build(BuildContext context) {
    Container topCon = Container(
      color: Colors.blue,
      constraints: BoxConstraints(minHeight: 20, maxHeight: 100),
    );
    Container bottomCon = Container(
      color: Colors.yellow,
      constraints: BoxConstraints(minHeight: 50, maxHeight: 100),
    );
    Widget topWidget = Flexible(
        child: topCon, fit: topTightFit ? FlexFit.tight : FlexFit.loose);
    Widget bottomWidget = topExpad ? Expanded(child: bottomCon) : bottomCon;

    return Scaffold(
      appBar: AppBar(
        title: Text("Flexible"),
        actions: [
          Center(
            child: Text("Top $tight"),
          ),
          IconButton(
              icon: Icon(Icons.expand_less),
              onPressed: () {
                setState(() {
                  tight = (topTightFit) ? "Tight" : "Loos";
                  topTightFit = !topTightFit;
                });
              }),
          Center(
            child: Text("Bottom $expanded"),
          ),
          IconButton(
              icon: Icon(Icons.expand_more),
              onPressed: () {
                setState(() {
                  topExpad = !topExpad;
                  expanded = topExpad ? "Expanded" : "Not";
                });
              })
        ],
      ),
      body: Column(
        children: [topWidget, bottomWidget],
      ),
    );
  }
}
