import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_excercise/drawer_screen/home_screen.dart';
import 'package:flutter_excercise/drawer_screen/notification_screen.dart';
import 'package:flutter_excercise/drawer_screen/user_profile.dart';

class ScaffoldWidget extends StatefulWidget {
  @override
  _ScaffoldWidgetState createState() => _ScaffoldWidgetState();
}

class _ScaffoldWidgetState extends State<ScaffoldWidget> {
  int currentIndex = 0;

  List<String> titleList = ["Home", "User Profile", "Notification"];
  getSelectScreen(int index) {
    switch (index) {
      case 0:
        return HomeScreen();
        break;
      case 1:
        return UserProfileScreen();
        break;
      case 2:
        return NotificationScreen();
        break;

      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${titleList[currentIndex]}"),
        actions: [
          IconButton(icon: Icon(Icons.mail), onPressed: showAlertEmail)
        ],
      ),
      body: getSelectScreen(currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        type: BottomNavigationBarType.fixed,
        onTap: (value) {
          setState(() {
            currentIndex = value;
          });
        },
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_outlined), label: "Profile"),
          BottomNavigationBarItem(
              icon: Icon(Icons.notification_important), label: "Notification"),
        ],
      ),
      persistentFooterButtons: [
        IconButton(icon: Icon(Icons.timer), onPressed: () {}),
        Text("Persistent Footer buttom")
      ],
      bottomSheet: Container(
        height: 50,
        color: Colors.yellow,
        alignment: Alignment.center,
        child: Row(
          children: [
            IconButton(icon: Icon(Icons.time_to_leave), onPressed: () {}),
            Text("Bottomsheet")
          ],
        ),
      ),
      // persistentFooterButtons: [
      //   ButtonBar(
      //     children: [
      //       ElevatedButton(onPressed: () {}, child: Text("one")),
      //       ElevatedButton(onPressed: () {}, child: Text("Two"))
      //     ],
      //   )
      // ],
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            Center(
                child: Column(
              children: [
                Container(
                  width: 150,
                  height: 150,
                  child: Image(
                    image: AssetImage("images/user_profile.jpg"),
                  ),
                ),
                Container(child: Text("User Name or Email Addresss")),
              ],
            )),
            ListTile(
              leading: Icon(Icons.people),
              title: Text("Home"),
              trailing: Icon(Icons.arrow_forward_ios_sharp),
              onTap: () {
                setState(() {
                  currentIndex = 0;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.account_circle_outlined),
              title: Text("User Profile"),
              trailing: Icon(Icons.arrow_forward_ios_sharp),
              onTap: () {
                print("tab user profile");
                setState(() {
                  currentIndex = 1;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.notifications_active_outlined),
              title: Text("Notification setting"),
              trailing: Icon(Icons.arrow_forward_ios_sharp),
              onTap: () {
                print("tab notification");
                setState(() {
                  currentIndex = 2;
                  Navigator.pop(context);
                });
              },
            ),
            Divider(
              height: 10.0,
              color: Colors.brown,
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text("Log out"),
              trailing: Icon(Icons.arrow_forward_ios_sharp),
              onTap: () {
                SystemNavigator.pop();
              },
            )
          ],
        ),
      ),
    );
  }

  showAlertEmail() async {
    return await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            backgroundColor: Colors.yellow[400],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            title: Row(children: [Icon(Icons.email), Text("Email")]),
            content: Container(
              child: Text("You email is sent successed!"),
            ),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("OK"))
            ]);
      },
    );
  }
}
