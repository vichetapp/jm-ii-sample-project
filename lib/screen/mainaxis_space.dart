import 'package:flutter/material.dart';

class MainAxisSpaceEvenly extends StatefulWidget {
  @override
  _MainAxisSpaceEvenlyState createState() => _MainAxisSpaceEvenlyState();
}

class _MainAxisSpaceEvenlyState extends State<MainAxisSpaceEvenly> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Space"),
        ),
        body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  alignment: Alignment.center,
                  width: 100,
                  height: 100,
                  // color: Colors.red,
                  decoration:
                      BoxDecoration(color: Colors.red, shape: BoxShape.circle),
                  child: Text("One"),
                ),
                Container(
                  alignment: Alignment.center,
                  width: 100,
                  height: 100,
                  // color: Colors.red,
                  decoration: BoxDecoration(
                      color: Colors.yellow, shape: BoxShape.circle),
                  child: Text("Two"),
                ),
                Container(
                  alignment: Alignment.center,
                  width: 100,
                  height: 100,
                  // color: Colors.red,
                  decoration: BoxDecoration(
                      color: Colors.green, shape: BoxShape.circle),
                  child: Text("Three"),
                )
              ]),
        ));
  }
}
