import 'package:flutter/material.dart';

class DialogWidget extends StatefulWidget {
  @override
  _DialogWidgetState createState() => _DialogWidgetState();
}

class _DialogWidgetState extends State<DialogWidget> {
  BoxFit boxfit = BoxFit.cover;
  String title = "BoxFit.cover";

  @override
  Widget build(BuildContext context) {
    List<Widget> listImage = [];
    for (int i = 0; i < 8; i++) {
      listImage.add(GridTile(
        child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Image.asset(
              "images/kitty0$i.jpg",
              fit: boxfit,
            )),
      ));
    }
    return Scaffold(
        appBar: AppBar(
          title: Text("Dialog"),
          actions: [
            ElevatedButton(
                onPressed: () {
                  showNewAlertDiloag();
                },
                child: Text("Alert Dialog")),
            ElevatedButton(
                onPressed: () {
                  showBoxFitDialog(context);
                },
                child: Text("BoxFit"))
          ],
        ),
        body: Container(child: OrientationBuilder(
          builder: (context, orientation) {
            return GridView.count(
              crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
              children: listImage,
            );
          },
        )));
  }

  showBoxFitDialog(BuildContext context) async {
    BoxFit box = await showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text("Simple Fitbox"),
          elevation: 20.0,
          backgroundColor: Colors.amber,
          children: [
            SimpleDialogOption(
              child: Text("Fitbox.cover"),
              onPressed: () {
                Navigator.pop(context, BoxFit.cover);
              },
            ),
            SimpleDialogOption(
              child: Text("Fitbox.fill"),
              onPressed: () {
                Navigator.pop(context, BoxFit.fill);
              },
            ),
            SimpleDialogOption(
              child: Text("Fitbox.fitHeight"),
              onPressed: () {
                Navigator.pop(context, BoxFit.fill);
              },
            ),
            SimpleDialogOption(
              child: Text("Fitbox.scaleDown"),
              onPressed: () {
                Navigator.pop(context, BoxFit.scaleDown);
              },
            ),
            SimpleDialogOption(
              child: Text("Fitbox.none"),
              onPressed: () {
                Navigator.pop(context, BoxFit.none);
              },
            )
          ],
        );
      },
    );
    if (box != null) {
      setState(() {
        boxfit = box;
      });
    }
  }

  showNewAlertDiloag() {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          title: Text(
            "Warning!",
            style: TextStyle(fontFamily: "Lucida", color: Colors.red),
          ),
          content: Text("Are you sure you want to delete it?"),
          contentPadding: EdgeInsets.all(8.0),
          contentTextStyle:
              TextStyle(color: Colors.orange[300], fontSize: 14.0),
          actionsPadding: EdgeInsets.only(bottom: 8.0),
          backgroundColor: Colors.green[50],
          buttonPadding: EdgeInsets.all(18.0),
          elevation: 50.0,
          clipBehavior: Clip.hardEdge,
          titleTextStyle: TextStyle(backgroundColor: Colors.amber),
          actions: [
            OutlinedButton(onPressed: () {}, child: Text("Yes")),
            OutlinedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("No"))
          ],
        );
      },
    );
  }
}
