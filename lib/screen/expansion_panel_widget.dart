import 'package:flutter/material.dart';

class ExpansionPanelListWidget extends StatefulWidget {
  @override
  _ExpansionPanelListWidgetState createState() =>
      _ExpansionPanelListWidgetState();
}

class _ExpansionPanelListWidgetState extends State<ExpansionPanelListWidget> {
  bool _isExpaned = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ExpanedPanel"),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(8.0),
        child: ExpansionPanelList(
            expansionCallback: (panelIndex, isExpanded) {
              setState(() {
                _isExpaned = !isExpanded;
              });
            },
            children: [
              ExpansionPanel(
                  isExpanded: _isExpaned,
                  headerBuilder: (context, isExpanded) {
                    return Padding(
                        padding: EdgeInsets.all(8.0), child: Text("Header"));
                  },
                  body: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("body"),
                  ))
            ]),
      ),
    );
  }
}
