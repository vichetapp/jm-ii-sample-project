import 'package:flutter/material.dart';
import 'package:flutter_excercise/api/crud_api.dart';

class ContainerWidget extends StatefulWidget {
  @override
  _ContainerWidgetState createState() => _ContainerWidgetState();
}

class _ContainerWidgetState extends State<ContainerWidget>
    with SingleTickerProviderStateMixin {
  AnimationController aniControl;
  Animation<double> animation;
  @override
  void initState() {
    aniControl =
        AnimationController(vsync: this, duration: Duration(seconds: 10));
    animation = Tween(begin: 0.0, end: 1.0).animate(aniControl)
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    CrudAPI c = CrudAPI();
    print("dot.. ${c..getUrl..getHttpAddress}");
    return Scaffold(
      appBar: AppBar(
        title: Text("Container"),
      ),
      body: Center(
        child: Container(
          child: RotationTransition(
            turns: AlwaysStoppedAnimation(animation.value),
            child: Icon(
              Icons.airplanemode_active,
              size: 140.0,
            ),
          ),
          decoration: BoxDecoration(
              border: Border.all(), borderRadius: BorderRadius.circular(20.0)),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          aniControl.forward(from: 0.0);
        },
        child: Icon(Icons.refresh),
      ),
    );
  }
}
