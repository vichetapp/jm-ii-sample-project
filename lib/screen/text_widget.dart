import 'package:flutter/material.dart';

class TextWidget extends StatefulWidget {
  @override
  _TextWidgetState createState() => _TextWidgetState();
}

class _TextWidgetState extends State<TextWidget> {
  int index = 0;
  List<TextBlock> list = [
    TextBlock(title: "Green Color", color: Colors.green),
    TextBlock(title: "Yellow Color", color: Colors.yellow),
    TextBlock(title: "Pink Color", color: Colors.pink),
    TextBlock(title: "Red Color", color: Colors.blue)
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TextWidget"),
      ),
      body: buildTextWidget(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (index < list.length) {
              index++;
            } else {
              index = 0;
            }
          });
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Widget buildTextWidget() {
    List<TextSpan> span = [];
    for (int i = 0; i < index; i++) {
      span.add(TextSpan(
          text: list[i].title,
          style: TextStyle(
              color: list[i].color,
              backgroundColor: Colors.amber,
              fontSize: 30.0,
              fontFamily: "Lucida",
              decoration: TextDecoration.underline)));
    }
    return Text.rich(TextSpan(children: span));
    // return Container(
    //     child: ListView.builder(
    //   itemCount: list.length,
    //   itemBuilder: (context, index) {
    //     return Container(
    //       child: Text(list[index].title),
    //     );
    //   },
    // ));
  }
}

class TextBlock {
  String title;
  Color color;
  TextBlock({this.title, this.color});
  String get getTitle => title;
  Color get getColor => this.color;
  set setTitle(String title) {
    this.title = title;
  }

  set setColor(Color color) {
    this.color = color;
  }
}
