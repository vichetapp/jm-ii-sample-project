import 'package:flutter/material.dart';

class SpacerWidget extends StatefulWidget {
  @override
  _SpacerWidgetState createState() => _SpacerWidgetState();
}

class _SpacerWidgetState extends State<SpacerWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Spacer"),
        ),
        body: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 150,
              child: Text("Hight 150"),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            Spacer(
              flex: 1,
            ),
            Container(
              alignment: Alignment.center,
              height: 150,
              child: Text("Hight 150"),
              decoration: BoxDecoration(color: Colors.red),
            ),
            Spacer(
              flex: 4,
            ),
            Container(
              alignment: Alignment.center,
              height: 150,
              child: Text("Hight 150"),
              decoration: BoxDecoration(color: Colors.yellow),
            ),
          ],
        ));
  }
}
