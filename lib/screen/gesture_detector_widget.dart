import 'package:flutter/material.dart';

class GestureDetectorWidget extends StatefulWidget {
  @override
  _GestureDetectorWidgetState createState() => _GestureDetectorWidgetState();
}

class _GestureDetectorWidgetState extends State<GestureDetectorWidget> {
  String del = '';
  String tab = '';
  String dTab = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("GestureDetector"),
        ),
        body: Column(
          children: [
            GestureDetector(
                child: Container(
                  constraints: BoxConstraints(minHeight: 50, maxHeight: 200),
                  color: Colors.green,
                ),
                onTap: () {
                  setState(() {
                    tab = "tabbed";
                  });
                },
                onTapDown: (detail) {
                  setState(() {
                    del = detail.localPosition.toString();
                  });
                }),
            Text(tab),
            Text(dTab),
            Text(del)
          ],
        ));
  }
}
