import 'package:flutter/material.dart';

class FormDetail extends StatefulWidget {
  @override
  _FormDetailState createState() => _FormDetailState();
}

class _FormDetailState extends State<FormDetail> {
  var globalKey = GlobalKey<FormState>();
  TextEditingController firstNameCon = TextEditingController();
  TextEditingController lastNameCon = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form"),
      ),
      body: Form(
        key: globalKey,
        child: ListView(
          children: [buildFirstName(), buildLastName(), buildSubmit()],
        ),
      ),
    );
  }

  Widget buildFirstName() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: TextFormField(
        controller: firstNameCon,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
            borderRadius: BorderRadius.circular(20.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
            borderRadius: BorderRadius.circular(20.0),
          ),
          labelText: "First Name",
          hintText: "Please enter first name!",
          // icon: Icon(Icons.person),
        ),
      ),
    );
  }

  Widget buildLastName() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: TextFormField(
        controller: lastNameCon,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
            borderRadius: BorderRadius.circular(20.0),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20.0),
              borderSide: BorderSide(color: Colors.black)),
          labelText: "Last Name",
          hintText: "Please enter last name!",
          // icon: Icon(Icons.person),
        ),
      ),
    );
  }

  buildSubmit() {
    return InkWell(
      onTap: () {},
      child: Container(
        child: Center(
            child: Text("Sign In",
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white))),
        constraints: BoxConstraints.expand(height: 60.0),
        margin: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0), color: Colors.blue),
      ),
    );
  }
}
