import 'dart:ui';

import 'package:flutter/material.dart';

class FlowerWidget extends StatefulWidget {
  @override
  _FlowerWidgetState createState() => _FlowerWidgetState();
}

class _FlowerWidgetState extends State<FlowerWidget> {
  double blur = 0;
  blurMore() {
    setState(() {
      blur += 5.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flower"),
        actions: [
          IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                setState(() {
                  blur = 0;
                });
              })
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(
                    "https://cdnpt01.viewbug.com/media/mediafiles/2015/07/05/56234977_large1300.jpg"),
                fit: BoxFit.cover)),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: blur, sigmaY: blur),
          child: Container(
            decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: blurMore,
        child: Icon(Icons.add),
      ),
    );
  }
}
