import 'package:flutter/material.dart';

class GridViewWidget extends StatefulWidget {
  @override
  _GridViewWidgetState createState() => _GridViewWidgetState();
}

class _GridViewWidgetState extends State<GridViewWidget> {
  List<Widget> listCat = [];
  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 8; i++) {
      listCat.add(GridTile(
          header: GridTileBar(
            title: Text("images/kitty0$i.jpg"),
            backgroundColor: Color.fromRGBO(0, 0, 0, 0.2),
          ),
          footer: GridTileBar(
            title: Text("How cute it is?"),
            backgroundColor: Color.fromRGBO(0, 0, 100, .5),
          ),
          child: Container(
            decoration: BoxDecoration(border: Border.all(width: 2.0)),
            child: Image.asset("images/kitty0$i.jpg"),
          )));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("GridView"),
        ),
        body: Container(
          // child: buildGridViewCount()
          child: buildGridViewBuilder(),
        ));
  }

  Widget buildGridViewBuilder() {
    return OrientationBuilder(
      builder: (context, orientation) {
        return GridView.builder(
          itemCount: listCat.length,
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemBuilder: (context, index) {
            return Card(
                // margin: EdgeInsets.all(8.0),
                child: GridTile(
                    header: GridTileBar(
                      backgroundColor: Color.fromRGBO(0, 100, 100, 0.5),
                      title: Text("images/kitty0$index.jpg"),
                    ),
                    footer: GridTileBar(
                      backgroundColor: Color.fromRGBO(0, 100, 0, 0.5),
                      title: Text("How cute is it?"),
                    ),
                    child: Image.asset(
                      "images/kitty0$index.jpg",
                      fit: BoxFit.cover,
                    )));
          },
        );
      },
    );
  }

  Widget buildGridViewCount() {
    return OrientationBuilder(
      builder: (context, orientation) {
        return GridView.count(
          crossAxisCount: orientation == Orientation.portrait ? 2 : 5,
          mainAxisSpacing: 2,
          padding: EdgeInsets.all(8.0),
          childAspectRatio: 1.0,
          children: listCat,
        );
      },
    );
  }
}
