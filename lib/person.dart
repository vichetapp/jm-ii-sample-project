class Person {
  String _firstName;
  String _lastName;
  String _gender;
  String _province;
  String _tel;
  DateTime _dob;
  double _rate;
  Map<String, bool> _certifcate;
  Person(
      [this._firstName,
      this._lastName,
      this._gender,
      this._province,
      this._tel,
      this._dob,
      this._certifcate,
      this._rate]);
  Person.empty() {
    _dob = DateTime.now();
    _rate = 0;
    this._certifcate = {"Bachelor ": false, "Master ": false, "Doctor ": false};
  }

  void setFirstName(String firstName) {
    this._firstName = firstName;
  }

  void setLastName(String lastName) {
    this._lastName = lastName;
  }

  void setGender(String gender) {
    this._gender = gender;
  }

  void setProvince(String province) {
    _province = province;
  }

  void setTel(String tel) {
    _tel = tel;
  }

  void setDob(DateTime dob) {
    _dob = dob;
  }

  void setCertificate(Map<String, bool> certificate) {
    this._certifcate = certificate;
  }

  void setRate(double rate) {
    this._rate = rate;
  }

  String get getFirstName => _firstName;
  String get getLastName => _lastName;
  String get getGender => _gender;
  String get getProvince => _province;
  String get getTel => _tel;
  DateTime get getDob => _dob;
  double get getRate => _rate;

  Map<String, bool> get getCertificate => _certifcate;
  String displayCertificate() {
    String msg = "";
    this._certifcate.forEach((key, value) {
      if (value == true) {
        msg += key;
      }
    });
    return msg;
  }

  @override
  String toString() {
    return "Your full name : $_firstName - $_lastName, " +
        "gender: $_gender, living in $_province, has contact number $_tel" +
        ", was bon in $_dob , you have certificat: " +
        displayCertificate() +
        " your rate is $_rate stars";
  }
}
