class Order {
  String _item;
  String _description;
  DateTime _date;
  double _total;
  Order(this._item, this._description, this._date, this._total);
  String get getItem => this._item;
  String get getDescription => this._description;
  DateTime get getDate => this._date;
  double get getTotal => this._total;
}
