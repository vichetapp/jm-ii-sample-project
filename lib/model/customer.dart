import 'package:flutter_excercise/model/order.dart';

class Customer {
  String _name;
  String _location;
  List<Order> _orderItems;
  Customer(this._name, this._location, this._orderItems);
  String get getName => this._name;
  String get getLocation => this._location;
  List<Order> get getOrderItems => this._orderItems;
}
