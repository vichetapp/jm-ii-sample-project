
class CRUDModel{
  String id;
  String email;
  String password;
  String avatar;

  CRUDModel({this.id,this.email,this.password,this.avatar});

  CRUDModel.fromJson(dynamic json){
    this.id = json['id'];
    this.email = json['email'];
    this.password = json['password'];
    this.avatar = json['avatar'];
  }
}