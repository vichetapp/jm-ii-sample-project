import 'package:flutter/material.dart';
import 'package:flutter_excercise/person.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class PersonWidget extends StatefulWidget {
  final Person people;
  PersonWidget({this.people});
  @override
  State<StatefulWidget> createState() {
    return PersonWidgetState(people);
  }
}

class PersonWidgetState extends State<PersonWidget> {
  var _globalKey = GlobalKey<FormState>();
  var firstNameCon = TextEditingController();
  var lastNameCon = TextEditingController();
  String gender = 'M';
  var genderCon = TextEditingController();
  String provinceSelected = "";
  var provinceCon = TextEditingController();
  var telCon = TextEditingController();
  String tel = '';
  var dobCon = TextEditingController();
  DateFormat df = DateFormat("dd MMM, yyyy");
  DateTime currentDate = DateTime.now();
  DateTime dobSelected;
  var listCerti = [];
  Map<String, bool> certificates;
  List<bool> answer = [];
  double rate = 0;
  var rateCon = TextEditingController();

  bool agreement = false;

  List<DropdownMenuItem<String>> provinceItems = [
    DropdownMenuItem(
      child: Text("Phnom Penh"),
      value: "PP",
    ),
    DropdownMenuItem(
      child: Text("Battam Bang"),
      value: "BB",
    ),
    DropdownMenuItem(
      child: Text("Kandal"),
      value: "KA",
    ),
    DropdownMenuItem(
      child: Text("Takeo"),
      value: "TA",
    ),
  ];

  PersonWidgetState(Person person) {
    gender = person.getGender;
    provinceSelected = person.getProvince;
    genderCon = TextEditingController(text: gender);
    provinceSelected = provinceItems[1].value;
    provinceCon = TextEditingController(text: provinceSelected);
    tel = person.getTel;
    telCon = TextEditingController(text: tel);
    dobSelected = person.getDob;
    dobCon = TextEditingController(
        text: person.getDob != null ? df.format(person.getDob) : "");
    // rate = person.getRate;
    certificates = person.getCertificate;
    print("certiifcate is $certificates");
    certificates.forEach((key, value) {
      listCerti.add(key);
      answer.add(value);
    });
    rate = person.getRate;
  }

  dynamic _onSave(Person person) {
    showDialog(
        context: context,
        builder: (buildeContext) {
          return AlertDialog(
            title: Row(
              children: [
                Icon(Icons.info, color: Colors.red),
                Text("Information", style: TextStyle(color: Colors.red))
              ],
            ),
            content: Text("You submited : ${person.toString()}"),
            backgroundColor: Colors.yellow[200],
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            actions: [
              ElevatedButton(
                  onPressed: () => {Navigator.pop(context, true)},
                  child: Text("Close"))
            ],
          );
        });
  }

  onRadioGenderChange(String selected) {
    setState(() {
      gender = selected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _globalKey,
        child: ListView(
          children: [
            buildFirstName(),
            buildLastName(),
            buildGenderWidget(),
            buildDropdownProvince(),
            buildTelWidget(),
            buildDobWidget(),
            buildCertificateWidget(),
            buildSmoothStarRating(),
            buildAgreementWidget(),
            buildSubmit()
          ],
        ));
  }

  Widget buildFirstName() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        validator: (value) {
          return (value.isEmpty) ? "Please enter first name?" : null;
        },
        controller: firstNameCon,
        decoration: InputDecoration(
          labelText: "First Name",
          hintText: "Please enter first name!",
          icon: Icon(Icons.person),
        ),
      ),
    );
  }

  Widget buildLastName() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        validator: (value) {
          return (value.isEmpty) ? "Please enter last name?" : null;
        },
        controller: lastNameCon,
        decoration: InputDecoration(
          labelText: "Last Name",
          hintText: "Please enter last name!",
          icon: Icon(Icons.person),
        ),
      ),
    );
  }

  Widget buildGenderWidget() {
    List<Widget> genderWidget = [
      Text("Male"),
      Radio(
        value: 'M',
        groupValue: gender,
        onChanged: (selected) {
          onRadioGenderChange(selected);
        },
      ),
      Text("Female"),
      Radio(
        value: 'F',
        groupValue: gender,
        onChanged: (selected) {
          onRadioGenderChange(selected);
        },
      )
    ];
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InputDecorator(
          decoration:
              InputDecoration(icon: Icon(Icons.person), labelText: "Gender"),
          child: Row(children: genderWidget)),
    );
  }

  Widget buildDropdownProvince() {
    DropdownButton<String> provinceOption = DropdownButton(
      items: provinceItems,
      value: provinceSelected,
      onChanged: (value) {
        setState(() {
          this.provinceSelected = value;
        });
      },
    );
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InputDecorator(
        decoration: InputDecoration(
            icon: Icon(Icons.location_city),
            labelText: "Province",
            hintText: "Select province!"),
        child: DropdownButtonHideUnderline(child: provinceOption),
      ),
    );
  }

  Widget buildTelWidget() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: telCon,
        validator: (telinput) {
          return (telinput.isEmpty) ? "Please enter phone number" : null;
        },
        onChanged: (value) {
          setState(() {
            tel = value;
          });
        },
        maxLength: 10,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          labelText: "Telephone:",
          icon: Icon(Icons.phone),
        ),
      ),
    );
  }

  Widget buildDobWidget() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DateTimePickerFormField(
        format: df,
        dateOnly:
            true, //if not input error Failed assertion: boolean expression must be not null
        initialDate: currentDate,
        controller: dobCon,
        inputType: InputType.date,
        validator: (dt) => (dt == null) ? "Please select date" : null,
        decoration: InputDecoration(
            icon: Icon(Icons.date_range), labelText: "Date of Birth"),
        onChanged: (value) {
          setState(() {
            dobSelected = value;
          });
        },
      ),
    );
  }

  Widget buildCertificateWidget() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InputDecorator(
          decoration: InputDecoration(
              icon: Icon(Icons.verified_user_outlined),
              labelText: "Certificate"),
          child: Container(
            height: 180.0,

            // color: Colors.green,
            child: ListView.builder(
                itemCount: listCerti.length,
                itemBuilder: (context, index) {
                  return CheckboxListTile(
                      title: Text(listCerti[index]),
                      value: answer[index],
                      onChanged: (selected) {
                        setState(() {
                          answer[index] = selected;
                          certificates[listCerti[index]] = selected;
                        });
                      });
                }),
          )),
    );
  }

  Widget buildSmoothStarRating() {
    return Padding(
        padding: EdgeInsets.all(8.0),
        child: InputDecorator(
            decoration: InputDecoration(
                icon: Icon(Icons.rate_review_outlined), labelText: "Rating"),
            child: SmoothStarRating(
              allowHalfRating: false,
              starCount: 5,
              size: 40.0,
              rating: rate,
              onRated: (rating) {
                setState(() {
                  rate = rating;
                });
              },
            )));
  }

  Widget buildAgreementWidget() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InputDecorator(
        decoration: InputDecoration(
            isDense: true,
            icon: Icon(Icons.contacts_rounded),
            labelText: "Terms & Conditions"),
        child: Column(
          children: [
            Text("Please check below to agree to the terms"),
            Row(
              children: [
                Checkbox(
                    value: agreement,
                    onChanged: (checked) {
                      setState(() {
                        agreement = checked;
                      });
                    }),
                Text("Agreement")
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildSubmit() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
          onPressed: agreement
              ? () {
                  if (_globalKey.currentState.validate()) {
                    widget.people.setFirstName(firstNameCon.text);
                    widget.people.setLastName(lastNameCon.text);
                    widget.people.setGender(gender);
                    widget.people.setProvince(provinceSelected);
                    widget.people.setTel(tel);
                    widget.people.setDob(df.parse(dobCon.text));
                    widget.people.setCertificate(certificates);
                    widget.people.setRate(rate);
                    _onSave(widget.people);
                  }
                }
              : null,
          child: Text("Submit")),
    );
  }
}
